import axios from 'axios'
import endpoint from './endpoint'

export default {
    getMovies: async () => {
        let returnData
        try {
            returnData = await axios(endpoint.GET_MOVIES())
        } catch (e) {
            // TODO Handle error
        }
        return returnData['data']
    },
    getMovieByID: async (id) => {
        let returnData
        try {
            returnData = await axios(endpoint.GET_MOVIE_BY_ID(id))
        } catch (e) {
            // TODO Handle error
        }
        return returnData['data']
    }
}