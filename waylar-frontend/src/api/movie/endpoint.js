export default {
    GET_MOVIES: () => {
        // TODO Parameterize
        return {
            method: 'get',
            url: `http://localhost:9000/movies`,
        }
    },
    GET_MOVIE_BY_ID: (id) => {
        // TODO Parameterize
        return {
            method: 'get',
            url: `http://localhost:9000/movie/${id}`
        }
    }
}