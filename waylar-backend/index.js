require('dotenv').config()

const Koa = require('koa')
const Kcors = require('kcors');


const router = require('./routes')
const logger = require('./middlewares/logger')


const app = new Koa()


// Config
const {
  CORS = 'true',
    PORT,
    HOST,
    NODE_ENV
} = process.env

if (CORS === 'true') {
  app.use(Kcors({
    origin: `*`,
    allowMethods: ['GET']
  }))
}

// Routing
router(app)



app.listen(Number(PORT) || 9000, HOST || '0.0.0.0', () => {
  logger.info(
    `SERVER Start at ${HOST} Port: ${PORT} with Environment: ${NODE_ENV}`
  )
})