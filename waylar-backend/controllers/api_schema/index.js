function ApiReturnSchema() {
    this.errorCode = null
    this.errorDescription = ''
    this.data = null
}

/*
 Params
 {
     errorCode
        Eg.
        XX -> Unknown error 
        01 -> Not Found
        02 -> No data provided
     errorDescription
     statusCode -> HTTP Status Code
     data
 }
*/


ApiReturnSchema.prototype.Success = function (koaContext, params) {
    this.data = params.data || null
    koaContext.status = 200
    koaContext.body = this
}

ApiReturnSchema.prototype.Fail = function (koaContext, params) {
    this.errorCode = params.errorCode || 'XX'
    this.errorDescription = params.errorDescription || 'Unknown Error'
    this.data = params.data || null

    koaContext.status = params.statusCode || 500
    koaContext.body = this
}


module.exports = ApiReturnSchema