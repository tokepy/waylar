const fs = require('fs')
const apiSchema = require('../api_schema')

module.exports = (ctx) => {
    let returnObj = new apiSchema()


    // Do some DB works
    let mockData = JSON.parse(fs.readFileSync(`${__dirname}/mockData.json`))

    // SIMULATE that the data is huge and need to show only id and movie title
    mockData = mockData.map(movie => {
        return {
            id: movie.id,
            movieName: movie.movieName
        }
    })

    // TODO Handle Error

    returnObj.Success(ctx, {
        data: mockData
    })
}