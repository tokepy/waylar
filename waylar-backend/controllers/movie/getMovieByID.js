const fs = require('fs')
const apiSchema = require('../api_schema')

module.exports = (ctx) => {
    let returnObj = new apiSchema()

    // Check Parameters
    let requestID = Number(ctx.params.id)

    if (isNaN(requestID)) {
        returnObj.Fail(ctx, {
            errorCode: '02',
            errorDescription: 'No ID is provided',
            statusCode: 400
        })
        return
    }


    // Do some DB works

    let mockData = JSON.parse(fs.readFileSync(`${__dirname}/mockData.json`))

    // SIMULATE Finding movie with certain ID
    for (let movie of mockData) {
        if (requestID === movie.id) {
            returnObj.Success(ctx, {
                data: movie
            })
            return
        }
    }

    // TODO Handle Error
    returnObj.Fail(ctx, {
        statusCode: 200,
        errorCode: '01',
        errorDescription: 'Not found any movie with ID requested'
    })
}