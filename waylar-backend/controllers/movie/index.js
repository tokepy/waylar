const getMovies = require('./getMovies')
const getMovieByID = require('./getMovieByID')

module.exports = {
    getMovies,
    getMovieByID
}