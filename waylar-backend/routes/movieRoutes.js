const movieControllers = require('../controllers/movie')

module.exports = [{
    method: 'GET',
    route: '/movies',
    handlers: [
        movieControllers.getMovies
    ]
}, {
    method: 'GET',
    route: '/movie/:id',
    handlers: [
        movieControllers.getMovieByID
    ]
}]