const Router = require('koa-router')
const routerInstance = new Router()

// Get all routes from each files
const allRoutes = [...require('./movieRoutes')]

for (let eachRoute of allRoutes) {
    let {
        method = '', route = '', handlers = []
    } = eachRoute
    routerInstance[method.toLowerCase()](route, ...handlers)
}


module.exports = (app) => {
    app.use(routerInstance.routes())
        .use(routerInstance.allowedMethods())
}