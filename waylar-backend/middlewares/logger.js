const bunyan = require('bunyan')

const logger = bunyan.createLogger({
    name: 'waylar-backend'
})

module.exports = logger